def default(this_header,params,last_header):
    if len(this_header) != len(last_header):
        return False,"The header is not correct."
	
    for i in range(len(this_header)):
        if this_header[i] != last_header[i]:
            return False,"The header is not correct."

    return True,""

		
def keyheader(this_header,params):
    loss_col = []
    for i in params:
        if not i in this_header:
            loss_col.append(i)
    
    if len(loss_col) > 0 :
        return False,"缺少欄位:"+",".join(loss_col)
    else:
        return True,""
    