import re
import time
import html.parser as htmlparser
import xlrd
import datetime


def lon_trans(source):
    strlon = str(source)
    if len(strlon) > 10:
        value = strlon[0:3] + '.' + strlon[3:9]+""+strlon[10:]
        return value
    else:
        value = strlon[0:3] + '.' + strlon[3:]
        return value


def lat_trans(source):
    strlat = str(source)
    if len(strlat) > 10:
        value = strlat[0:2] + '.' + strlat[2:8]+""+strlat[9:]
        return value
    else:
        value = strlat[0:2] + '.' + strlat[2:]
        return value


def change_dateformat(source):
    value = source[0:4] + '-' + source[4:6] + '-' + source[6:8]
    return value


def change_dateformat2(source):
    value = source[0:4] + '-' + source[5:7] + '-' + source[8:10]
    return value


def getPart(source, part_id, file):
	return file[part_id]


def polygonuse(source):
    b = '\"' + source + '\"'
    return b


def yearmonth(source):
    stra = str(source)
    if len(stra) == 4:
        month = stra[-2:]
        year = stra[0:2]
        year1 = int(year) + 1911
        output = str(year1) + str(month)

    elif len(stra) == 5:
        month = stra[-2:]
        year = stra[0:3]
        year1 = int(year) + 1911
        output = str(year1) + str(month)
    else:
        output = stra

    return output


def eco_replace(source):
    value = re.sub(r"\s+", "", source)
    value = value.replace("p", "")
    value = value.replace("r", "")
    return value


def day_format(source):
    test = str(source)
    if test != '':
        stra = str(source)
        year = stra.split('年')[0]
        other = stra.split('年')[1]
        year1 = int(year) + 1911
        other1 = other.replace('月', '-')
        other1 = other1.replace('日', '')
        output = str(year1) + '-' + str(other1)
    else:
        output = source
    return output


def day_format2(source):
    if len(source) == 7:
        day = source[-2:]
        month = source[-4:-2]
        year = source[0:3]
        year1 = int(year) + 1911
        output = str(year1)+'-'+str(month)+'-'+str(day)
    elif len(source) == 6:
        day = source[-2:]
        month = source[-4:-2]
        year = source[0:2]
        year1 = int(year) + 1911
        output = str(year1)+'-'+str(month)+'-'+str(day)

    else:
        output = source

    return output


def day_format3(source):
    test = str(source)
    if test != '':
        if '年' in test:
            test2 = test.replace('年', '/')
            test2 = test2.replace('月', '/')
            test2 = test2.replace('日', '/')
        else:
            test2 = test

        year = test2.split('/')[0]
        month = test2.split('/')[1]
        day = test2.split('/')[2]

        lenth = len(year)
        if test2 != '' and lenth == 4:
            output = str(year)+'-'+str(month)+'-'+str(day)
        elif test2 != '' and lenth <= 3:
            year1 = int(year) + 1911
            output = str(year1)+'-'+str(month)+'-'+str(day)
        else:
            output = test
    else:
        output = test

    return output


def address_split(source):
    value = source[5:]
    return value


def address_split2(source):
    value = source[7:]
    return value


def address_merge(source1, source2):
    value = source1+source2
    return value


def address_merge2(source1, source2, source3):
    value = source1+source2+source3
    return value


def address_split3(source):
    c = '.'
    data = []
    for pos,char in enumerate(source):
        if(char == c): 
             data.append('號')        
             break
        else:
            data.append(char)
    value="".join(map(str, data))
    return value

def address_split4(source):
    a = '、'
    b = ','
    c ='.'
    d='，'
    
    data = []
    for pos,char in enumerate(source):
        if(char == a or char==b or char==c or char==d):   
            data.append('號')
            break
        else:
            data.append(char)
    value="".join(map(str, data))
    value = value[3:]
    return value

def old_address_split(source):
    c = '('
    d ='（'
    data = []

    if(source.rfind(c)==11 or source.rfind(d)==11 ):
        for pos,char in enumerate(source):
            if(char == c or char == d):          
                data.append(source[pos-3:pos])  
        data.append(source[16:])
    elif(source.rfind(c)==3 or source.rfind(d)==3 ):
        for pos,char in enumerate(source):
            if(char == c or char == d):          
                data.append(source[pos-3:pos])  
        data.append(source[8:])    
    else:
        data.append(source)

    value="".join(map(str, data))
    return value

def hilife(source1, source2,source3):   
    if(source1=="萊爾富國際股份有限公司"):
         if(source3=='01'):
            value=source2
    return value

def landname_merge(source1, source2):
    if source1[-1]=='段':
        source1=source1[:-1]
             
    if source2 =="":
        value = source1+"段"
    else:
        value = source1+"段"+source2+"小段"
    return value

def landid_check(source):
    if len(source)==1:
        value ="000"+source
    elif len(source) == 2:
        value="00"+source
    elif len(source)==3:
        value ="0"+source
    else:
        value=source
    return value

def check_name(source):
    value = re.sub("環保局","",source)
    return value